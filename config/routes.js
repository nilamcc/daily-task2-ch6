const express = require("express");
const controllers = require("../app/controllers");

const appRouter = express.Router();
const apiRouter = express.Router();

/** Mount GET / handler */
appRouter.get("/", controllers.main.index);

/**
 * TODO: Implement your own API
 *       implementations
 */
//API FOR DEALERS
apiRouter.get("/api/v1/dealers", controllers.api.v1.dealers.list);
apiRouter.post("/api/v1/dealers", controllers.api.v1.dealers.create);
apiRouter.put(
  "/api/v1/dealers/:id",
  controllers.api.v1.dealers.setdealers,
  controllers.api.v1.dealers.update
);
apiRouter.get(
  "/api/v1/dealers/:id",
  controllers.api.v1.dealers.setdealers,
  controllers.api.v1.dealers.show
);
apiRouter.delete(
  "/api/v1/dealers/:id",
  controllers.api.v1.dealers.setdealers,
  controllers.api.v1.dealers.destroy
);

//API FOR CARS
apiRouter.get("/api/v1/cars", controllers.api.v1.cars.list);
apiRouter.post("/api/v1/cars", controllers.api.v1.cars.create);
apiRouter.put(
  "/api/v1/cars/:id",
  controllers.api.v1.cars.setcars,
  controllers.api.v1.cars.update
);
apiRouter.get(
  "/api/v1/cars/:id",
  controllers.api.v1.cars.setcars,
  controllers.api.v1.cars.show
);
apiRouter.delete(
  "/api/v1/cars/:id",
  controllers.api.v1.cars.setcars,
  controllers.api.v1.cars.destroy
);
/**
 * TODO: Delete this, this is just a demonstration of
 *       error handler
 */
apiRouter.get("/api/v1/errors", () => {
  throw new Error(
    "The Industrial Revolution and its consequences have been a disaster for the human race."
  );
});

apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

/**
 * TODO: Delete this, this is just a demonstration of
 *       error handler
 */
appRouter.get("/errors", () => {
  throw new Error(
    "The Industrial Revolution and its consequences have been a disaster for the human race."
  );
});

appRouter.use(apiRouter);

/** Mount Not Found Handler */
appRouter.use(controllers.main.onLost);

/** Mount Exception Handler */
appRouter.use(controllers.main.onError);

module.exports = appRouter;
