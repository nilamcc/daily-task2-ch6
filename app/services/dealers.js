const {Dealers} = require("../models")

module.exports = {
    getAll() {
        return Dealers.findAll();
    },
    create(name,location,contact) {
        return Dealers.create({
            name,
            location,
            contact
        });
    },
    update(Dealers, requestBody) {
        return Dealers.update(requestBody);
    },

    choose(id) {
        return Dealers.findByPk(id);
    },
    delete(Dealers) {
        return Dealers.destroy();
    },

}