const {Cars} = require("../models")

module.exports = {
    getAll() {
        return Cars.findAll();
    },
    create(merk,color,country,dealerId) {
        return Cars.create({
            merk,
            color,
            country,
            dealerId
        });
    },
    update(Cars, requestBody) {
        return Cars.update(requestBody);
    },

    choose(id) {
        return Cars.findByPk(id);
    },
    delete(Cars) {
        return Cars.destroy();
    },

}