const { Dealers } = require("../../../models/dealers");
const { Cars } = require("../../../models/cars")
const dealersService = require("../../../services/dealers");
    
module.exports = {
    list(req, res) {
        dealersService.getAll({
            include: {
            model: Cars
            },
        })
        .then((dealers) => {
            res.status(200).json({
                status: "OK",
                data: {
                    dealers,
                },
            });
        })
        .catch((err) => {
            res.status(400).json({
                status: "FAIL",
                message: err.message,
            });
        });
    },
    
    create(req, res) {
        const { name, location, contact } = req.body;
        dealersService.create(name, location, contact)
        .then((dealers) => {
            res.status(201).json({
            status: "OK! dealers created succesfully",
            data: dealers,
            });
        })
        .catch((err) => {
            res.status(201).json({
            status: "FAIL",
            message: err.message,
            });
        });
    },
    
    update(req, res) {
        dealersService.update(req.dealers, req.body)
        .then((dealers) => {
            res.status(200).json({
                status: "OK! dealers changed succesfully",
                data: dealers,
            });
        })
        .catch((err) => {
            res.status(422).json({
                status: "FAIL",
                message: err.message,
            });
        });
    },
    
    show(req, res) {
        const dealers = req.dealers;    
        res.status(200).json({
            status: "OK",
            data: dealers,
        });
    },
    
    destroy(req, res) {
        dealersService.delete(req.dealers)
        .then(() => {
            res.status(204).end();
        })
        .catch((err) => {
            res.status(422).json({
                status: "FAIL",
                message: err.message,
            });
        });
    },
    
    setdealers(req, res, next) {
        dealersService.choose(req.params.id)
            .then((dealers) => {
            if (!dealers) {
                res.status(404).json({
                    status: "FAIL",
                    message: "dealers not found!",
                });
                return;
            }
    
            req.dealers = dealers;
            next();
            })
            .catch((err) => {
            res.status(404).json({
                status: "FAIL",
                message: "dealers not found!",
            });
            });
        },
    };
    