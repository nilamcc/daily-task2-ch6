const { Cars, Dealers } = require("../../../models");
const carsService = require("../../../services/cars");

module.exports = {
    list(req, res) {
        carsService.getAll({
            include: {
                model: Dealers
            },
        })
        .then((cars) => {
            res.status(200).json({
                status: "OK",
                data: {
                    cars,
                },
            });
        })
        .catch((err) => {
            res.status(400).json({
                status: "FAIL",
                message: err.message,
            });
        });
    },
 
    create(req, res) {
        const {
        merk,
        color,
        country,
        dealerId
        } = req.body;
        carsService
        .create(merk, color, country, dealerId)
        .then((cars) => {
            res.status(201).json({
                status: "OK! cars added succesfully",
                data: cars,
            });
        })
        .catch((err) => {
            res.status(201).json({
                status: "FAIL",
                message: err.message,
            });
        });
    },
    
    update(req, res) {
        carsService
        .update(req.cars, req.body)
        .then((cars) => {
            res.status(200).json({
                status: "OK! cars changed succesfully",
                data: cars,
            });
        })
        .catch((err) => {
            res.status(422).json({
                status: "FAIL",
                message: err.message,
            });
        });
    },
    
    show(req, res) {
        const cars = req.cars;
        res.status(200).json({
            status: "OK",
            data: cars,
        });
    },
    
    destroy(req, res) {
        carsService
        .delete(req.cars)
        .then(() => {
            res.status(204).end();
        })
        .catch((err) => {
            res.status(422).json({
                status: "FAIL",
                message: err.message,
            });
        });
    },
    
    setcars(req, res, next) {
        carsService
        .choose(req.params.id)
        .then((cars) => {
            if (!cars) {
            res.status(404).json({
                status: "FAIL",
                message: "cars not found!",
            });
            return;
            }
            req.cars = cars;
            next();
        })
        .catch((err) => {
            res.status(404).json({
                status: "FAIL",
                message: "cars not found!",
            });
        });
    },
    
};