const dealers = require("./dealers")
const cars = require("./cars")

module.exports = {
  cars,
  dealers,
};
