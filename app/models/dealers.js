'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Dealers extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Dealers.hasMany(models.Cars, {
        foreignKey: 'dealerId'
      })
    }
  }
  Dealers.init({
    name: DataTypes.STRING,
    location: DataTypes.STRING,
    contact: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Dealers',
  });
  return Dealers;
};